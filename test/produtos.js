var express = require('../config/express')();
var request = require('supertest')(express);

describe('#ProdutosController',function(){
	it('#Listagem json',function(done){

		request.get('/produtos/lista')
		.set('Accept','application/json')
		.expect('Content-Type',/json/)
		.expect(200,done);
	});

	it('#Cadastro de novo produto com dados inválidos', function(done){
		request.post('/produtos')
		.send({titulo:"",descricao:"Desc livro", preco:"a"})
		.expect(400,done);
	});

	it('#Cadastro de novo produto com dados válidos', function(done){
		request.post('/produtos')
		.send({titulo:"Titulo Válido",descricao:"Desc Válida",preco:"10"})
		.expect(302,done);
	});
});