var mysql = require('mysql');
//Factory Method
function createDBConnection(){
	return mysql.createConnection({
			host : 'localhost',
			user : 'thales',
			password : 'thales',
			database : 'casadocodigo_nodejs'
	});
}

//wrapper
module.exports=function(){
	return createDBConnection;
}